
## Установка через docker

- **Запустить докер: docker-compose up -d**
- **Открыть в браузере [http://localhost:8888](http://localhost:8888)**
- **Загрузка XML-файла [http://localhost:8888/load](http://localhost:8888/load)**
- **Вывод данных [http://localhost:8888/goods](http://localhost:8888/goods)**
- **Кнопка &lt;Download&gt; формирует excel-файл и загружает его**

#### Build with laravel latest version 10.39
