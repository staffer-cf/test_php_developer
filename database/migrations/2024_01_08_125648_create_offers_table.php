<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('ext_offer_id', 30)->default('');
            $table->boolean('available')->default(false);

            $table->integer('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');

            $table->string('currency_id',3);
            $table->string('name', 100);
            $table->string('picture', 500)->default('');
            $table->string('url', 500)->default('');
            $table->unsignedDecimal('price');
            $table->unsignedDecimal('old_price');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};
