<?php

use App\Http\Controllers\ExportDataController;
use App\Http\Controllers\GoodsController;
use App\Http\Controllers\LoadDataController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/goods', GoodsController::class);



Route::get('/test', [TestController::class, 'index']);

Route::get('/load', LoadDataController::class)->name('load.data');
Route::get('/export', ExportDataController::class)->name('export.excel');

