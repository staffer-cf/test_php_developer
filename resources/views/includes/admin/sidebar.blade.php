<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->


        <li class="nav-item">
            <a href="/goods" class="nav-link @if (request()->getRequestUri() === '/goods') active @endif">
                <i class="fas fa-table nav-icon"></i>
                <p>Goods</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="/load" class="nav-link @if (request()->getRequestUri() === '/load') active @endif">
                <i class="fas fa-angle-right nav-icon"></i>
                <p>Load</p>
            </a>
        </li>

    </ul>
</nav>
