@section('block-buttons')
    <div class="pt-3 pb-3 pl-2 pr-2">
        <a href="{{ route('export.excel') }}" class="btn btn-primary">Download &lt;Excel&gt;</a>
    </div>
@endsection
