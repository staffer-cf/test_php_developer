@section('table')

{{--    {{ dump($categories) }}--}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- /.card -->

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Goods</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Главная категория</th>
                                <th>Текущая категория</th>
                                <th>Наименование</th>
                                <th>Фото</th>
                                <th>Цена</th>
                                <th>Валюта</th>
                                <th>Ссылка</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)

                                <tr>
                                    <td>{{ $item['parent_category'] }}</td>
                                    <td>{{ $item['current_category'] }}</td>
                                    <td>{{ $item['title'] }}</td>
                                    <td><img class="img-size-64" alt="picture" src="{{ $item['picture'] }}" /></td>
                                    <td>{{ $item['price'] }}<br><span style="text-decoration: line-through;" class="font-italic text-gray-dark">{{ $item['old_price'] }}</span></td>
                                    <td>{{ $item['currency'] }}</td>
                                    <td>{{ $item['url'] }}</td>

                                </tr>

                            @endforeach

                            </tbody>

                            <tfoot>
                            <tr>
                                <th>Главная категория</th>
                                <th>Текущая категория</th>
                                <th>Наименование</th>
                                <th>Фото</th>
                                <th>Цена</th>
                                <th>Валюта</th>
                                <th>Ссылка</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
@endsection
