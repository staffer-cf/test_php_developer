@extends('layout.admin.index')

@include('goods.table')
@include('goods.block-buttons')

@section('content')

    @yield('block-buttons')
    @yield('table')

@endsection
