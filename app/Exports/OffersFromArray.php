<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class OffersFromArray implements FromArray
{

    /**
     * @return array
     */
    protected $invoices;

    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }
}
