<?php

namespace App\Exports;

use App\Models\Offer;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
//use Laravel\Scout\Builder as ScoutBuilder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

class OffersFromQuery implements FromQuery
{

    /**
     * @return Builder|EloquentBuilder|Relation
     */

    use Exportable;

    public function query()
    {
        return Offer::query()
            ->select('category_id','name','price','');
    }
}
