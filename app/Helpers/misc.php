<?php

use App\Models\Category;


if (!function_exists('getCategoryName')) {
    function getCategoryName($category_id) {
        return Category::where('id', $category_id)->first()->name;
    }
}


if (!defined('URL_LINK_XML')) {
    define('URL_LINK_XML', 'https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml');
}



