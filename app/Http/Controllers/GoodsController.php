<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Offer;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    public function __invoke(Request $request)
    {

        $data = [];

        foreach (Offer::all() as $item) {
            $data[] = [
                'parent_category' => getCategoryName($item->category->parent_id),
                'current_category' => $item->category->name,
                'title' => $item->name,
                'picture' => $item->picture,
                'price' => $item->price,
                'old_price' => $item->old_price,
                'currency' => $item->currency_id,
                'url' => $item->url,
            ];
        }

//        dump($data);

        return view('goods.index', compact('data'));
    }
}
