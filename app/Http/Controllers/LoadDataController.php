<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Offer;
use Illuminate\Http\Request;
use Orchestra\Parser\Xml\Facade as XmlParser;

class LoadDataController extends Controller
{
    public function __invoke(Request $request)
    {

        $xml = XmlParser::load(URL_LINK_XML);

        // add categories to the table 'categories'
        foreach ($xml?->getContent()?->shop?->categories?->category as $item) {
            Category::query()->updateOrCreate([
                'id' => (int)$item['id'],
                'parent_id' => (int)$item['parentId'],
                'name' => (string)$item,
            ]);
        }

        // add goods to the table 'offers'
        foreach ($xml?->getContent()?->shop?->offers?->offer as $item) {

            Offer::query()->updateOrCreate([
                'ext_offer_id' => (string)$item['id'],
                'available' => (boolean)$item['available'] ?? false,

                'category_id' => (int)$item->categoryId ?? 0,
                'currency_id' => (string)$item->currencyId,
                'name' => (string)$item->name,
                'picture' => (string)$item->picture,
                'url' => (string)$item->url,
                'price' => (float)$item->price,
                'old_price' => (float)$item->oldprice,
            ]);
        }

        $arr = [];

        session()->flash('success', 'Операция выполнена успешно!');
//        session()->flash('error', 'Ошибка добавления / обновления');

//        echo 'Items were added/updated to the database';
        return view('goods.load', compact('arr'));
    }
}
