<?php

namespace App\Http\Controllers;

use App\Exports\OffersFromArray;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ExportDataController extends Controller
{
    public function __invoke(Request $request)
    {

        $export_data = [];

        $export_data[] = [
//            'ParentId',
            'Главная категория',
//            'CategoryId',
            'Подкатегория',
            'Наименование',
            'Цена',
            'Валюта',
            'Ссылка',
        ];

        foreach (Offer::all() as $item) {
            $export_data[] = [
//                $item->category->parent_id,
                getCategoryName($item->category->parent_id),
//                $item->category_id,
                $item->category->name,
                $item->name,
                $item->price,
                $item->currency_id,
                $item->url,
            ];
        }


        $export = new OffersFromArray([
            $export_data
        ]);

        $dt = Carbon::now()->format('Ymd_His');
        return Excel::download($export, "offers_$dt.xlsx");
    }
}
