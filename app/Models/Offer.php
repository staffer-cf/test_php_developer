<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Offer extends Model
{
    use HasFactory;

//    protected $table = 'offers';

    protected $fillable = [
        'ext_offer_id',
        'available',
        'category_id',
        'currency_id',
        'name',
        'picture',
        'url',
        'price',
        'old_price',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
